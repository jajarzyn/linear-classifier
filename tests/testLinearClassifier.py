import pytest
import numpy as np
import os
import shutil
from contextlib import nullcontext as does_not_raise

from classifier.LinearClassifier import fit_binary_classifier, LinearClassifier
from utilities.RandomDataGenerator import random_data_generator, WrongInputData

X_binary = np.array([[1, 1],
                     [1, 0.8],
                     [-1, -0.8],
                     [-1, -1]])
y_binary = np.array([0, 0, 1, 1])

X = np.array([[1, 1],
             [1, 0.8],
             [-1, -0.8],
             [-1, -1],
             [1, -1],
             [1, -0.8],
             [-1, 1],
             [-1, 0.8]])
y = np.array([0, 0, 1, 1, 2, 2, 3, 3])

radom_data_generator_cases = [(100, 2, 2, 0, does_not_raise()),
                              (100, 2, 2, 0.5, does_not_raise()),
                              (100, 2, 2, 1, does_not_raise()),
                              (100, 5, 2, 0.25, does_not_raise()),
                              (100, 2, 5, 0.25, does_not_raise()),
                              (1, 2, 2, 0.5, pytest.raises(WrongInputData)),
                              (100, 2, 2, 3, pytest.raises(WrongInputData))]


SAVE_DIR = os.path.join(os.getcwd(), 'figs')


def teardown_module():
    shutil.rmtree(SAVE_DIR, ignore_errors=True)


@pytest.mark.parametrize("X, y, precalc", [(X_binary, y_binary, True),
                                           (X_binary, y_binary, False)])
def test_fit_binary_classifier(X, y, precalc):
    sep_plane = fit_binary_classifier(X, y, precalc_sepplane=precalc, no_experiments=1)
    print(sep_plane)
    assert sep_plane.shape[0] == 3


@pytest.mark.parametrize("n_samples, n_classes, n_dimension, p, expectation", radom_data_generator_cases)
def test_random_data_generator(n_samples, n_classes, n_dimension, p, expectation):
    with expectation:
        X, y = random_data_generator(n_samples, n_classes, n_dimension, p=p)
        assert X.shape == (n_samples, n_dimension)
        assert y.shape[0] == n_samples
        assert len(np.unique(y)) == n_classes


class TestLinearClassifier:
    @pytest.mark.parametrize("X, y, vote_type", [(X, y, 'ovr'),
                                                 (X, y, 'ovo'),
                                                 (X, y, 'all')])
    def test_fit(self, X, y, vote_type):
        cls = LinearClassifier()
        cls.fit(X, y, vote_type=vote_type, no_experiments=1)
        assert cls.was_fitted

    def test_vote(self):
        assert True

    @pytest.mark.parametrize("vote_type", ['ovo', 'ovr'])
    def test_predict_ovr(self, vote_type):
        cls = LinearClassifier()
        cls.vote_type.append(vote_type)
        cls.sep_planes[vote_type] = {}
        if vote_type == 'ovr':
            cls.sep_planes[vote_type][0] = np.array([1., 1., 0.])
            cls.sep_planes[vote_type][1] = np.array([-1., -1., 0.])
        elif vote_type == 'ovo':
            cls.sep_planes[vote_type][(0, 1)] = np.array([1., 1., 0.])
        cls.labels = np.array([0, 1])
        cls.was_fitted = True
        y_pred = cls.predict(X_binary, vote_type)
        assert all(y_pred == y_binary)


@pytest.mark.parametrize("seed", [0, 1, 2, 3])
def test_e2e(seed):
    size = 500
    cls_ovo = LinearClassifier()
    cls_ovr = LinearClassifier()

    X, y = random_data_generator(size, 3, 2, 1, seed=seed)
    X_train = X[:int(size / 2), :]
    X_test = X[int(size / 2):, :]
    y_train = y[:int(size / 2)]
    y_test = y[int(size / 2):]

    cls_ovo.fit(X_train, y_train, vote_type='ovo', no_experiments=1)
    cls_ovr.fit(X_train, y_train, vote_type='ovr', no_experiments=1)

    os.makedirs('figs', exist_ok=True)
    cls_ovo.visualize(X_test, y_test, 'ovo', plot=True, save_name='ovo.png', view=False)
    cls_ovr.visualize(X_test, y_test, 'ovr', plot=True, save_name='ovr.png', view=False)

    plots = ['ovo.png', 'ovr.png']
    assert all(plot in os.listdir(SAVE_DIR) for plot in plots)
    assert isinstance(cls_ovo.accuracy, float)
