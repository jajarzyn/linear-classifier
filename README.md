# Linear Classifier

Clone repository and go to the project directory.
Setup virtual environtment

`mkdir venv
python3 -m venv venv/`

Activate virtual environment

`source venv/bin/activate`

Install requirements

`pip install -r requirements.txt`
