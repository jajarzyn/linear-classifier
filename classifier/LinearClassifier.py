import numpy as np
import matplotlib.pyplot as plt
from itertools import combinations
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, confusion_matrix
import os


class LinearClassifier:
    def __init__(self):
        self.was_fitted = False
        self.vote_type = []
        self.sep_planes = {}
        self.labels = None
        self.conf_matrix, self.accuracy, self.recall, self.precision, self.f1 = None, None, None, None, None

    def fit(self, X: np.ndarray, y: np.array, vote_type: str, *args, **kwargs):
        """
        classifier fitting method
        :param X: data
        :param y: data_labels
        :param vote_type: ovo, ovr, all - one vs. one, one vs. all, both
        :return: self
        """
        if vote_type not in ['ovo', 'ovr', 'all']:
            raise NotImplementedError("Only ovo and ovr voting commuties are supported")
        self.labels = np.unique(y)
        if vote_type in ['ovo', 'all']:
            if 'ovo' not in self.vote_type:
                self.vote_type.append('ovo')
            self.sep_planes['ovo'] = {}
            label_pairs = combinations(self.labels, 2)
            for label_pair in label_pairs:
                label_pair_mask = (y == label_pair[0]) | (y == label_pair[1])
                ovo_X = X[label_pair_mask, :]
                ovo_y = y[label_pair_mask]
                self.sep_planes['ovo'][label_pair] = fit_binary_classifier(ovo_X, ovo_y, **kwargs)
        elif vote_type in ['ovr', 'all']:
            if 'ovr' not in self.vote_type:
                self.vote_type.append('ovr')
            self.sep_planes['ovr'] = {}
            for label in self.labels:
                ovr_y = np.array([0 if i == label else 1 for i in y])
                self.sep_planes['ovr'][label] = fit_binary_classifier(X, ovr_y, **kwargs)
        else:
            raise NotImplementedError("Only ovo and ovr voting commuties are supported")
        self.was_fitted = True
        return self

    def predict(self, X: np.ndarray, vote_type: str) -> np.array:
        """
        classifier predict data method
        :param X: data
        :param vote_type: ovr, ovo
        :return: predict labels
        """
        assert self.was_fitted, 'classifier was not fitted yet'
        if vote_type not in ['ovo', 'ovr']:
            raise NotImplementedError("Only ovo and ovr voting commuties are supported")
        assert vote_type in self.vote_type, f'{vote_type} not fitted yet'

        reject_label = np.max(self.labels) + 1  # label for undecided vote

        # voting method - choose most popular
        votes = self.get_votes(X, vote_type)
        max_votes = np.max(votes, axis=1)  # selecting labels with most no. votes
        y_pred = np.argmax(votes, axis=1)
        max_votes_2 = np.sort(votes, axis=1)[:, -2]
        y_pred[max_votes - max_votes_2 == 0] = reject_label  # set undecided label if no most voted label

        return y_pred

    def restore_classifier(self):
        raise NotImplementedError('Restoring model not implemented yet')

    def get_votes(self, X:np.array, vote_type: str) -> np.array:
        """
        :param X: data
        :param vote_type: ovo, ovr
        :return: voting result matrix: with rows: [class 0 votes, class 1 votes ..., class n votes]
        """
        votes = np.zeros([X.shape[0], len(self.labels)], dtype=int)
        X_vote = np.concatenate([X, np.ones([X.shape[0], 1])], axis=1)
        for labels in self.sep_planes[vote_type]:
            result = X_vote.dot(self.sep_planes[vote_type][labels])
            if vote_type == 'ovr':
                votes[result > 0, labels] += 1
            elif vote_type == 'ovo':
                votes[result > 0, labels[0]] += 1
                votes[result < 0, labels[1]] += 1
        return votes

    def visualize(self, X: np.ndarray, y: np.ndarray, vote_type: str, plot=False, save_name=None, view=True):
        """
        predict + visualize results method
        creates score metrics: [accuracy, precision, recall, f1] as attributes
        :param X: data
        :param y: correst labels
        :param vote_type: ovo, ovr
        :param plot: bool, whether to draw plot, to be saved in figs dir
        :param save_name: str, name of plot
        :param view: bool, verbosity of plot
        """
        if vote_type not in ['ovo', 'ovr']:
            raise NotImplementedError("Only ovo and ovr voting commuties are supported")
        if plot:
            assert save_name is not None, 'plot save_name not specified eg. bubensz.png'
        y_pred = self.predict(X, vote_type)

        self.conf_matrix = confusion_matrix(y, y_pred)
        self.accuracy = accuracy_score(y, y_pred)

        self.recall, self.precision, self.f1 = {}, {}, {}
        self.accuracy = accuracy_score(y, y_pred)
        for label in self.labels:
            y_true_label = [1 if i == label else 0 for i in y]
            y_pred_label = [1 if i == label else 0 for i in y_pred]
            self.recall[label] = recall_score(y_true_label, y_pred_label, pos_label=1)
            self.precision[label] = precision_score(y_true_label, y_pred_label, pos_label=1)
            self.f1[label] = f1_score(y_true_label, y_pred_label, pos_label=1)

        if plot:
            colors = ['b', 'g', 'r', 'c', 'm', 'y']
            max_colors = max(len(self.labels), len(self.sep_planes[vote_type]))
            if max_colors > len(colors):
                for i in range(max_colors - len(colors)):
                    colors.append(tuple(np.random.random(3)))
            for i, label in enumerate(self.labels):
                plt.scatter(X[y == label, 0], X[y == label, 1], color=colors[i], label=f'label {label}')
            for i, label in enumerate(self.sep_planes[vote_type]):
                plane = self.sep_planes[vote_type][label]
                xp = [-1, 1]
                yp = [(plane[0] - plane[-1]) / plane[1], (-plane[0] - plane[-1]) / plane[1]]
                plt.plot(xp, yp, color=colors[i], label=f'{label} cls')
        plt.xlim(-1, 1)
        plt.ylim(-1, 1)
        plt.legend()
        if view and plot:
            plt.show()
        plt.savefig(os.path.join(os.getcwd(), 'figs', save_name))


def fit_binary_classifier(X:np.array, y:np.array,
                          precalc_sepplane=True, no_experiments=5, max_iter=30) -> np.array:
    assert len(np.unique(y)) == 2, 'labels must be split into 2 categories'
    assert X.shape[0] == y.shape[0], 'data and labels must have same length'

    class_labels = np.unique(y)  # [positive class, negative class]
    eps = X.shape[0] / 100
    planes = []
    errs = []

    coef = [1 if i == class_labels[0] else -1 for i in y]
    coef = np.array(coef, dtype=int).reshape(-1, 1)  # coef 1 for label_0 and -1 for label_1

    X_coef = X.copy()
    X_coef[y == class_labels[1], :] *= -1
    X_coef = np.concatenate((X_coef, coef), axis=1)

    for i in range(no_experiments):
        if precalc_sepplane:
            temp_vec = X[y == class_labels[1]][i, :] - X[y == class_labels[0]][i, :]  # random neg -> pos vector
            temp_point = (X[y == class_labels[0]][i, :] + X[y == class_labels[1]][i, :]) / 2
            sep_plane = np.append(temp_vec, - temp_vec.dot(temp_point))
        else:
            sep_plane = np.random.rand(X.shape[1] + 1) * 2 - 1

        # fit
        learn = 2
        update = sum(X_coef[X_coef.dot(sep_plane) < 0, :])
        sep_plane += learn * update

        for _ in range(max_iter):
            prev_plane = sep_plane
            prev_update = update
            update = sum(X_coef[X_coef.dot(sep_plane) < 0, :])
            err = update if isinstance(update, int) else update.dot(update)

            if isinstance(update, int):  # all classes separated correctly
                planes.append(sep_plane)
                errs.append(err)
                break
            if update.dot(update) < eps:  # error margin achieved
                planes.append(sep_plane)
                errs.append(err)
                break
            sep_plane += learn * update
            if update.dot(update) / prev_update.dot(prev_update) > 1:
                sep_plane = prev_plane
                learn *= 0.8
                update = prev_update
            elif update.dot(update) / prev_update.dot(prev_update) == 1:
                learn *= 0.8
            else:
                learn /= np.sqrt(update.dot(update) / prev_update.dot(prev_update))  # lowering learning rate
        planes.append(sep_plane)
        errs.append(err)
    idx = errs.index(min(errs))
    sep_plane = planes[idx]

    return sep_plane
