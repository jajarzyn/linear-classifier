import numpy as np
from typing import Tuple


class WrongInputData(Exception):
    pass


def random_data_generator(n_samples: int, n_classes: int, n_dimension: int, p=1., seed=None) -> Tuple[np.array, np.array]:
    """
    Function for random sample generation with _p_ factor of linearly separatable classes split
    :param n_samples: No. samples to be generated
    :param n_classes: No. classes
    :param n_dimension: No. data dimensions
    :param p: linear separatibility factor
    :param seed: random sample seed
    :return: data and data labels
    """
    if n_samples < n_classes:
        raise WrongInputData("No. samples mustn't be lower than no. classes")
    if p < 0 or p > 1:
        raise WrongInputData('p factor must belong to <0, 1>')
    # clustering_points = np.random.random([n_classes, n_dimension])
    if seed is not None:
        np.random.seed(seed)
    X = np.random.random([n_samples, n_dimension]) * 2 - 1
    clustering_points = X[:n_classes, :]
    y = None
    for i in range(n_classes):
        dist = X - clustering_points[i, :]
        dist = (p * dist + (1 - p) * (np.random.random([n_samples, n_dimension]) * 2 + 1))
        dist = np.sum(dist * dist, axis=1).reshape([-1, 1])
        if y is None:
            y = dist
        else:
            y = np.concatenate([y, dist], axis=1)
    y = np.argmin(y, axis=1)

    if len(np.unique(y)) < n_classes:
        X, y = random_data_generator(n_samples, n_classes, n_dimension, p=p)

    return X, y


